package com.test.notification.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
@RequiredArgsConstructor
public class RabbitMqConfig {

    public static final String TOPIC_EXCHANGE_DRIVER = "driver.exchange";
    public static final String TOPIC_EXCHANGE_DEPARTMENT = "department.exchange";

    public static final String QUEUE_DRIVER_REGISTRATION = "driver.registration";
    public static final String QUEUE_DRIVER_DELETE = "driver.delete";
    public static final String QUEUE_DEPARTMENT_REGISTRATION = "department.registration";
    public static final String QUEUE_DEPARTMENT_DELETE = "department.delete";

    public static final String ROUTING_KEY_DRIVER_REGISTRATION = "key.driver.registration";
    public static final String ROUTING_KEY_DRIVER_DELETE = "key.driver.delete";
    public static final String ROUTING_KEY_DEPARTMENT_REGISTRATION = "key.department.registration";
    public static final String ROUTING_KEY_DEPARTMENT_DELETE = "key.department.delete";

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Bean
    public TopicExchange driverExchange() {
        return new TopicExchange(TOPIC_EXCHANGE_DRIVER);
    }

    @Bean
    public TopicExchange departmentExchange() {
        return new TopicExchange(TOPIC_EXCHANGE_DEPARTMENT);
    }

    @Bean
    public Queue queueDriverRegistration() {
        return new Queue(QUEUE_DRIVER_REGISTRATION);
    }

    @Bean
    public Queue queueDriverDelete() {
        return new Queue(QUEUE_DRIVER_DELETE);
    }

    @Bean
    public Queue queueDepartmentRegistration() {
        return new Queue(QUEUE_DEPARTMENT_REGISTRATION);
    }

    @Bean
    public Queue queueDepartmentDelete() {
        return new Queue(QUEUE_DEPARTMENT_DELETE);
    }

    @Bean
    public Binding driverBindingRegistration() {
        return BindingBuilder
                .bind(queueDriverRegistration())
                .to(driverExchange())
                .with(ROUTING_KEY_DRIVER_REGISTRATION);
    }

    @Bean
    public Binding driverBindingDelete() {
        return BindingBuilder
                .bind(queueDriverDelete())
                .to(driverExchange())
                .with(ROUTING_KEY_DRIVER_DELETE);
    }

    @Bean
    public Binding departmentBindingRegistration() {
        return BindingBuilder
                .bind(queueDepartmentRegistration())
                .to(departmentExchange())
                .with(ROUTING_KEY_DEPARTMENT_REGISTRATION);
    }

    @Bean
    public Binding departmentBindingDelete() {
        return BindingBuilder
                .bind(queueDepartmentDelete())
                .to(departmentExchange())
                .with(ROUTING_KEY_DEPARTMENT_DELETE);
    }

}
