package com.test.notification.config;

import org.springframework.amqp.core.Message;

public interface RabbitHandler {

    void notificationsRegistration(Message message);

    void notificationsDelete(Message message);

}
