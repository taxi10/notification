package com.test.notification.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.notification.config.RabbitHandler;
import com.test.notification.config.RabbitMqConfig;
import com.test.notification.dto.response.DepartmentResponseDTO;
import com.test.notification.dto.response.DriverResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DepartmentMessageHandler implements RabbitHandler {

    private final JavaMailSender javaMailSender;

    @Override
    @SneakyThrows
    @RabbitListener(queues = RabbitMqConfig.QUEUE_DEPARTMENT_REGISTRATION)
    public void notificationsRegistration(Message message) {
        byte[] body = message.getBody();
        String json = new String(body);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        DepartmentResponseDTO departmentResponseDTO = objectMapper.readValue(json, DepartmentResponseDTO.class);


        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(departmentResponseDTO.getEmail());
        mailMessage.setSubject("Registration");
        mailMessage.setText("You have successfully registered as a Company.");

        javaMailSender.send(mailMessage);

        log.debug("A message about the successful registration of the company was sent to "
                + departmentResponseDTO.getEmail() + ".");
    }

    @Override
    @SneakyThrows
    @RabbitListener(queues = RabbitMqConfig.QUEUE_DEPARTMENT_DELETE)
    public void notificationsDelete(Message message) {
        byte[] body = message.getBody();

        String email = new String(body);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Delete");
        mailMessage.setText("Your account has been deleted.");

        javaMailSender.send(mailMessage);

        log.debug("The company's account was deleted by email: " + email);
    }
}
