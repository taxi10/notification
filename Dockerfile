FROM openjdk:11-jre
#Открываем порт в docker container
EXPOSE 9999
#Создаем переменную которая хранит путь до jar файла
ARG JAR_FILE=build/libs/notification-1.0.jar
#Копируем jar файл в docker container
ADD ${JAR_FILE} notification.jar
#Запускам jar файл
ENTRYPOINT ["java","-jar","notification.jar"]